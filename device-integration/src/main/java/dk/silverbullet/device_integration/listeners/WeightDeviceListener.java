package dk.silverbullet.device_integration.listeners;

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andweightscale.bluetooth_smart.AndWeightScaleBroadcastReceiver;
import dk.silverbullet.device_integration.devices.andweightscale.continua.AndWeightScaleController;
import dk.silverbullet.device_integration.devices.andweightscale.Weight;
import dk.silverbullet.device_integration.devices.andweightscale.Weight.Unit;
import dk.silverbullet.device_integration.protocols.continua.ContinuaDeviceController;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.android.AndroidHdpController;

public class WeightDeviceListener implements DeviceListener<Weight> {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(WeightDeviceListener.class);

    private static final Pattern DEVICE_NAME_PATTERN = Pattern.compile("(" +
            "(A(&|N)D WS UC-321PBT-C.*)|" +
            "(A(&|N)D UC-351PBT-Ci.*)|" +
            "(A(&|N)D_UC-352BLE.*)" +
            ")");

    private static final Pattern DEVICE_ADDRESS_PATTERN = Pattern.compile("(" +
            "(00:09:1F:.*)|" +
            "(5C:31:3E:.*)" +
            ")");

    private final Activity activity;
    private DefaultDeviceListener defaultDeviceListener;
    private OnEventListener onEventListener;

    // --*-- Constructors --*--

    public WeightDeviceListener(Activity activity,
                                OnEventListener onEventListener) {
        this.activity = activity;
        this.onEventListener = onEventListener;
        this.defaultDeviceListener =
                new DefaultDeviceListener(activity, onEventListener);
    }

    // --*-- Methods --*--

    @Override
    public void start() {

        AndroidHdpController androidHdpController =
                new AndroidHdpController(activity.getApplicationContext());
        ContinuaDeviceController andWeightScaleController =
                new AndWeightScaleController(this, androidHdpController);
        AndWeightScaleBroadcastReceiver andWeightScaleBroadcastReceiver =
                new AndWeightScaleBroadcastReceiver(this);

        defaultDeviceListener.connect(andWeightScaleController,
                andWeightScaleBroadcastReceiver,
                BluetoothClass.Device.HEALTH_WEIGHING, DEVICE_ADDRESS_PATTERN,
                DEVICE_NAME_PATTERN);
    }

    @Override
    public void stop() {
        defaultDeviceListener.stop();
    }

    @Override
    public void connecting() {
        Log.d(TAG, "Connecting");
        defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.WEIGHT_CONNECTING);
    }

    @Override
    public void connected() {
        defaultDeviceListener.connected();
    }

    @Override
    public void disconnected() {
        defaultDeviceListener.disconnected();
    }

    @Override
    public void permanentProblem() {
        defaultDeviceListener.permanentProblem();
    }

    @Override
    public void temporaryProblem() {
        defaultDeviceListener.temporaryProblem();
    }

    @Override
    public void measurementReceived(final DeviceInformation deviceInformation, final Weight weight) {
        Log.d(TAG, "Measurement received!");

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (weight.getUnit() == Unit.KG) {
                    try {
                        JSONObject measurementEvent = createMeasurementEvent(deviceInformation, weight);
                        onEventListener.onEvent(measurementEvent);
                        disconnected();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject event = defaultDeviceListener.createStatusEvent(
                                JSONConstants.INFO, JSONConstants.WEIGHT_CHANGE_UNIT);
                        onEventListener.onEvent(event);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
    }

    private JSONObject createMeasurementEvent(DeviceInformation deviceInformation,
                                              Weight weight) throws JSONException {

        JSONObject event = new JSONObject();
        Date eventTimestamp = new Date();
        event.put(JSONConstants.TIMESTAMP, eventTimestamp.toString());
        event.put(JSONConstants.DEVICE, defaultDeviceListener.createDeviceObject(deviceInformation));
        event.put(JSONConstants.TYPE, JSONConstants.MEASUREMENT);
        JSONObject measurement = new JSONObject();
        measurement.put(JSONConstants.TYPE, JSONConstants.WEIGHT);
        measurement.put(JSONConstants.UNIT, weight.getUnit().getName());
        measurement.put(JSONConstants.VALUE, round(weight.getWeight(), 2));
        event.put(JSONConstants.MEASUREMENT, measurement);
        return event;
    }

    private double round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

}
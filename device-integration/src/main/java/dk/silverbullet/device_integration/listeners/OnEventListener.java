package dk.silverbullet.device_integration.listeners;

import org.json.JSONException;
import org.json.JSONObject;

public interface OnEventListener {

    void onEvent(JSONObject jsonObject) throws JSONException;

}

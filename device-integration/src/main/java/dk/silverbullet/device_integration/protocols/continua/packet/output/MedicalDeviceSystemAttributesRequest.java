package dk.silverbullet.device_integration.protocols.continua.packet.output;

public class MedicalDeviceSystemAttributesRequest implements OutputPacket {

    // --*-- Fields --*--

    private final int invokeId;

    // --*-- Constructors --*-

    public MedicalDeviceSystemAttributesRequest(int invokeId) {
        this.invokeId = invokeId;
    }

    // --*-- Methods --*--

    @Override
    public byte[] getContents() {
        OrderedByteWriter writer = new OrderedByteWriter();

        writer.writeShort(0xE700); // APDU CHOICE Type (AareApdu)
        writer.writeShort(0x000E); // CHOICE.length = 14
        writer.writeShort(0x000C); // OCTET STRING.length = 12
        writer.writeShort(invokeId); // invoke-id (differentiates this message from any other outstanding, choice is implementation specific)
        writer.writeShort(0x0103); // CHOICE (Remote Operation Invoke | Get)
        writer.writeShort(0x0006); // CHOICE.length = 6
        writer.writeShort(0x0000); // handle = 0 (MDS object)
        writer.writeShort(0x0000); // attribute-id-list.count = 0 (all attributes)
        writer.writeShort(0x0000); // attribute-id-list.length = 0

        return writer.getBytes();
    }

    @Override
    public String toString() {
        return "MedicalDeviceSystemAttributesRequest [invokeId=" + invokeId + "]";
    }

}

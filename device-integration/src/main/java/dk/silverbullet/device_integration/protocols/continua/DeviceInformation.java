package dk.silverbullet.device_integration.protocols.continua;

/**
 * Encapsulates the device information.
 */
public class DeviceInformation {

    // --*-- Fields --*--

    private String model;
    private String eui64;
    private String firmwareRevision;
    private String manufacturer;
    private String serialNumber;
    private String systemId;

    // --*-- Constructors --*--

    public DeviceInformation() {}

    public DeviceInformation(String model, String eui64,
                             String firmwareRevision, String manufacturer,
                             String serialNumber, String systemId) {
        this.model = model;
        this.eui64 = eui64;
        this.firmwareRevision = firmwareRevision;
        this.manufacturer = manufacturer;
        this.serialNumber = serialNumber;
        this.systemId = systemId;
    }

    // --*-- Methods --*--

    @Override
    public String toString() {
        return "DeviceInformation" +
                " [model=" + model +
                ", eui64=" + eui64 +
                ", firmwareRevision=" + firmwareRevision +
                ", manufacturer=" + manufacturer +
                ", serialNumber=" + serialNumber +
                ", systemId=" + systemId +
                "]";
    }

    // -*- Getters -*-

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEui64() {
        return eui64;
    }

    public void setEui64(String eui64) {
        this.eui64 = eui64;
    }

    public String getFirmwareRevision() {
        return firmwareRevision;
    }

    public void setFirmwareRevision(String firmwareRevision) {
        this.firmwareRevision = firmwareRevision;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

}

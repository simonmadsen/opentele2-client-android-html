package dk.silverbullet.device_integration.protocols.continua.packet.input;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateController;
import dk.silverbullet.device_integration.protocols.continua.ContinuaPacketTag;

public abstract class ConfirmedMeasurementDataPacket extends InputPacket {

    private static final String TAG = "ConfMeasurePacket";

    public static final int MDC_ATTRIBUTES = -1; // magic number
    public static final int MDC_NOTI_CONFIG = 0x0D1C;
    public static final int MDC_NOTI_SCAN_REPORT_FIXED = 0x0D1D;
    public static final int MDC_NOTI_SCAN_REPORT_VAR = 0x0D1E;
    public static final int MDC_NOTI_SCAN_REPORT_MP_FIXED = 0x0D1F;

    private int invokeId;
    private int eventTime;
    private int eventType;

    public ConfirmedMeasurementDataPacket(byte[] contents) throws IOException {
        super(ContinuaPacketTag.PRST_APDU, contents);
        checkContents();
    }

    public ConfirmedMeasurementDataPacket(int invokeId, int eventTime) {
        super(ContinuaPacketTag.PRST_APDU, new byte[0]);
        this.invokeId = invokeId;
        this.eventTime = eventTime;
    }

    protected abstract void readObjects(int eventType, OrderedByteReader byteReader) throws IOException;

    private void checkContents() throws IOException {
        OrderedByteReader in = new OrderedByteReader(super.getContents());

        in.readShort(); // OCTET STRING.length - 52 for Nonin oxymeter
        invokeId = in.readShort(); // invoke-id (differentiates this from other outstanding messages)
        Log.d(TAG, "Invoke ID read: "  + invokeId + ", " + String.format("%02X", invokeId));
        in.readShort(); // CHOICE(Remote Operation Invoke | Confirmed Event Report) - 0x0100 for Nonin Oxymeter
        in.readShort(); // CHOICE.length - 46 for Nonin Oxymeter
        checkShort(in, "obj-handle = 0 (MDS object)", 0);
        if (invokeId == ProtocolStateController.MEDICAL_DEVICE_SYSTEM_ATTRIBUTES_INVOKE_ID) {
            readObjects(MDC_ATTRIBUTES, in);
        } else {
            eventTime = in.readInt();
            eventType = in.readShort(); // event-type
            in.readShort(); // event-info.length - 36 for Nonin Oxymeter
            if (eventType == MDC_NOTI_CONFIG) {
                checkShort(in, "ScanReportInfoFixed.data-req-id", ProtocolStateController.EXTENDED_CONFIGURATION);
            } else {
                checkShort(in, "ScanReportInfoFixed.data-req-id", 0xF000);
                in.readShort(); // ScanReportInfoFixed.scan-report-no
            }

            readObjects(eventType, in);
        }


        if (in.available() != 0) {
            Log.w("ConfirmedPacket", "Superfluous data in packet: " + in.available() + " bytes");
            ArrayList<Integer> contents = new ArrayList<>();
            for (int i = 0; i < in.available(); i++) {
                int content = in.readByte();
                contents.add(content);
            }
            Log.d(TAG, contents.toString());
            // throw new UnexpectedPacketFormatException("Superfluous data in packet: " + in.available() + " bytes");
        }

        in.close();
    }

    public int getInvokeId() {
        return invokeId;
    }

    public int getEventTime() {
        return eventTime;
    }

    public int getEventType() {
        return eventType;
    }
}

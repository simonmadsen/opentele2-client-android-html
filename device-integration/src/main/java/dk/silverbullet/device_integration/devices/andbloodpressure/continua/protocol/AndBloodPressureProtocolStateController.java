package dk.silverbullet.device_integration.devices.andbloodpressure.continua.protocol;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andbloodpressure.BloodPressureAndPulse;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.packet.input.AssociationReleaseRequestPacket;
import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateController;
import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateListener;
import dk.silverbullet.device_integration.devices.andbloodpressure.continua.packet.input.AndBloodPressureConfirmedMeasurementDataPacket;

/**
 * Implementation of the protocol specific for the A&amp;D blood pressure device.
 */
public class AndBloodPressureProtocolStateController extends
        ProtocolStateController<BloodPressureAndPulse, AndBloodPressureConfirmedMeasurementDataPacket> {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndBloodPressureProtocolStateController.class);

    private static final int NO_TIME = -1;

    private long timestampForBloodPressure = NO_TIME;
    private int systolicBloodPressure;
    private int diastolicBloodPressure;
    private int meanArterialPressure;

    private long timestampForPulse = NO_TIME;
    private int pulse;
    private DeviceInformation deviceInformation;

    // --*-- Constructors --*--

    public AndBloodPressureProtocolStateController(ProtocolStateListener<BloodPressureAndPulse> listener) {
        super(listener);
    }

    // --*-- Methods --*--

    @Override
    protected AndBloodPressureConfirmedMeasurementDataPacket createConfirmedMeasurementsType(byte[] contents)
            throws IOException {
        return new AndBloodPressureConfirmedMeasurementDataPacket(contents);
    }

    @Override
    protected void handleConfirmedMeasurement(AndBloodPressureConfirmedMeasurementDataPacket confirmedMeasurement) {

        if (confirmedMeasurement.hasDeviceInformation()) {
            deviceInformation = confirmedMeasurement.getDeviceInformation();
            deviceInformation.setSystemId(systemId.asString());
            Log.d(TAG, "Setting deviceInformation on BloodPressureStateController: " + deviceInformation);
        }

        if (confirmedMeasurement.hasBloodPressure() &&
                confirmedMeasurement.getBloodPressureTimestamp() > timestampForBloodPressure) {
            systolicBloodPressure = confirmedMeasurement.getSystolicBloodPressure();
            diastolicBloodPressure = confirmedMeasurement.getDiastolicBloodPressure();
            meanArterialPressure = confirmedMeasurement.getMeanArterialPressure();
            timestampForBloodPressure = confirmedMeasurement.getBloodPressureTimestamp();
        }

        if (confirmedMeasurement.hasPulse() && confirmedMeasurement.getPulseTimestamp() > timestampForPulse) {
            pulse = confirmedMeasurement.getPulse();
            timestampForPulse = confirmedMeasurement.getPulseTimestamp();
        }
    }

    @Override
    protected void handleAssociationReleaseRequest(AssociationReleaseRequestPacket associationReleaseRequestPacket) {

        if (deviceInformation == null) {
            deviceInformation = new DeviceInformation();
            deviceInformation.setSystemId(systemId.asString());
        }

        if (timestampForBloodPressure == timestampForPulse && timestampForBloodPressure != NO_TIME) {
            listener.measurementReceived(deviceInformation, new BloodPressureAndPulse(systolicBloodPressure,
                    diastolicBloodPressure, meanArterialPressure, pulse));
        } else {
            listener.noMeasurementsReceived();
        }
    }

}

package dk.silverbullet.device_integration.devices.noninsaturation.packet.states;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninPacketCollector;

public abstract class ReceiverState {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(ReceiverState.class);

    public static final byte STX = 0x2;
    public static final byte ETX = 0x3;
    public static final byte ACK = 0x6;

    protected final NoninPacketCollector stateController;

    // --*-- Constructors --*--

    public ReceiverState(NoninPacketCollector stateController) {
        this.stateController = stateController;
    }

    // --*-- Methods --*--

    public abstract boolean receive(int in);

    // Function called when the state is entered
    public abstract void entering();

}

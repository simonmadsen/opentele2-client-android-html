package dk.silverbullet.device_integration.devices.andbloodpressure;

public class BloodPressureAndPulse {

    // --*-- Fields --*--

    private final int systolic;
    private final int diastolic;
    private final int meanArterialPressure;
    private final int pulse;

    // --*-- Constructors --*--

    public BloodPressureAndPulse(int systolic, int diastolic,
                                 int meanArterialPressure, int pulse) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.meanArterialPressure = meanArterialPressure;
        this.pulse = pulse;
    }

    // --*-- Methods --*--

    public int getSystolic() {
        return systolic;
    }

    public int getDiastolic() {
        return diastolic;
    }

    public int getMeanArterialPressure() {
        return meanArterialPressure;
    }

    public int getPulse() {
        return pulse;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + diastolic;
        result = prime * result + meanArterialPressure;
        result = prime * result + pulse;
        result = prime * result + systolic;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BloodPressureAndPulse other = (BloodPressureAndPulse) obj;
        return diastolic == other.diastolic &&
                meanArterialPressure == other.meanArterialPressure &&
                pulse == other.pulse &&
                systolic == other.systolic;
    }

    @Override
    public String toString() {
        return "BloodPressureAndPulse" +
                " [systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", meanArterialPressure=" + meanArterialPressure +
                ", pulse=" + pulse +
                "]";
    }
}

package dk.silverbullet.device_integration.devices.noninsaturation.packet.states;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninMeasurementPacket;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninPacketCollector;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninPacketFactory;

public class MeasurementDataState extends ReceiverState {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(MeasurementDataState.class);

    private NoninPacketCollector noninPacketCollector;

    // --*-- Constructors --*--

    public MeasurementDataState(NoninPacketCollector noninPacketCollector) {
        super(noninPacketCollector);
        this.noninPacketCollector = noninPacketCollector;
    }

    // --*-- Methods --*--

    @Override
    public boolean receive(int in) {
        stateController.addInt(in);
        Log.d(TAG, in + "");
        try {
            // TODO: Add some code that brings the read in sync, ie. MSB set to one for first packet
            int length = noninPacketCollector.getRead().length;
            // First byte must have MSB set, the reset must have it cleared
            if ((1 == length &&  in < 0x80) || (1 < length && in >= 0x80)) {
                return false;
            }

            if (length == 4) {  //Each measurement is 4 bytes long.
                NoninMeasurementPacket measurementPacket =
                        NoninPacketFactory.measurementPacket(noninPacketCollector.getRead());
                noninPacketCollector.clearBuffer();
                noninPacketCollector.addMeasurement(measurementPacket);
            }

            return true;
        } catch (IOException e) {
            Log.e(TAG, "Could not parse measurement", e);
        }
        return false;
    }

    @Override
    public void entering()
    {

    }
}

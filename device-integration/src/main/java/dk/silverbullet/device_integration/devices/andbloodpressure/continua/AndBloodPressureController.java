package dk.silverbullet.device_integration.devices.andbloodpressure.continua;

import java.util.regex.Pattern;

import dk.silverbullet.device_integration.devices.andbloodpressure.BloodPressureAndPulse;
import dk.silverbullet.device_integration.devices.andbloodpressure.continua.protocol.AndBloodPressureProtocolStateController;
import dk.silverbullet.device_integration.protocols.continua.AbstractDeviceController;
import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.protocols.continua.HdpController;
import dk.silverbullet.device_integration.protocols.continua.HdpProfile;
import dk.silverbullet.device_integration.protocols.continua.PacketCollector;
import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;

/**
 * Main entry point to communicating with the A&D Medical blood pressure device.
 */
public class AndBloodPressureController extends AbstractDeviceController<BloodPressureAndPulse> {

    // --*-- Constructors --*--

    /**
     * @param listener Object given callbacks for e.g. notifying the user about
     *                 the progress through the GUI.
     * @param hdpController
     *            The specific Bluetooth controller to use for the underlying
     *            Bluetooth communication. See {@see AndroidBluetoothController}.
     */
    public AndBloodPressureController(DeviceListener<BloodPressureAndPulse> listener,
                                       HdpController hdpController) {
        super(listener, hdpController, HdpProfile.BLOOD_PRESSURE_METER);

        hdpController.setPacketCollector(new PacketCollector(
                new AndBloodPressureProtocolStateController(this)));
    }

    // --*-- Methods --*--

    /**
     * @param deviceNamePattern -
     * @param macAddressPattern -
     *
     * @throws DeviceInitialisationException if Bluetooth is not available, not
     * enabled, or if a general Bluetooth error occurs.
     */
    @Override
    public void initiate(Pattern deviceNamePattern, Pattern macAddressPattern)
            throws DeviceInitialisationException {
        listener.connecting();
        hdpController.initiate(deviceNamePattern, macAddressPattern);
    }

}

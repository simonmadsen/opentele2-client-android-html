package dk.silverbullet.telemed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import dk.silverbullet.telemed.devices.DeviceHandler;
import dk.silverbullet.telemed.reflection.ReflectionHelper;
import dk.silverbullet.telemed.video.VideoConference;
import dk.silverbullet.telemed.webview.AppLoaded;
import dk.silverbullet.telemed.webview.Html5WebView;
import dk.silverbullet.telemed.webview.WebViewHelper;
import dk.silverbullet.telemed.webview.html5tonative.ClearQuestionnaireReminderRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.DeviceInformationRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.DeviceMeasurementRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.ErrorPageInterface;
import dk.silverbullet.telemed.webview.html5tonative.JavaScriptInterface;
import dk.silverbullet.telemed.webview.html5tonative.JsonEventHub;
import dk.silverbullet.telemed.webview.html5tonative.OpenUrlRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.OverdueQuestionnairesRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.PatientPrivacyPolicyRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.SetupRemindersRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.StartNotificationSoundRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.StartVideoConferenceRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.StopDeviceMeasurementRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.StopNotificationSoundRequestHandler;
import dk.silverbullet.telemed.webview.html5tonative.VideoEnabledRequestHandler;
import dk.silverbullet.telemed.webview.nativetohtml5.Html5Hook;
import dk.silverbullet.telemed.webview.nativetohtml5.Html5WebViewMessageDispatcher;
import dk.silverbullet.telemed.webview.nativetohtml5.LifeCycleEvents;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

/**
 * Instantiates the state of the app and loads the OpenTele client HTML5 page.
 *
 * @author Peter Urbak
 * @version 2015-04-28
 */
public class MainActivity extends Activity {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(MainActivity.class);

    private static final String MENU_FRAGMENT = "#/menu";
    private static final String CREATE_VIDEO_FRAGMENT = "createVideoFragment";
    private static boolean hasBeenCreated;

    private boolean isInCreateFlow;
    private boolean videoInProgress;
    private Html5WebView webView;
    private String openTeleHtml5Url;
    private Ringtone ringtone;
    private LifeCycleEvents lifeCycleEvents;
    private JsonEventHub eventHub;
    private WebViewMessageDispatcher webViewMessageDispatcher;

    // --*-- Methods --*--

    /**
     * {@inheritDoc}
     */
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isInCreateFlow = true;

        setContentView(R.layout.activity_main);
        hasBeenCreated = true;

        Properties config = new ConfigProperties(this.getApplicationContext()).getConfig();
        openTeleHtml5Url = config.getProperty(Constants.OPEN_TELE_HTML5_URL);

        initializeWebView();
        startWebView();
    }

    private void initializeWebView() {
        webView = (Html5WebView) findViewById(R.id.webView);
        eventHub = new JsonEventHub();
        lifeCycleEvents = new LifeCycleEvents(webView);

        WebViewHelper webViewHelper = new WebViewHelper();
        List<JavaScriptInterface> javaScriptInterfaces = generateJavaScriptInterfaces();
        webViewHelper.initializeWebView(javaScriptInterfaces, webView, eventHub, new AppLoaded() {
            @Override
            public void loaded(String url) {
                if (!url.startsWith(openTeleHtml5Url)) {
                    return;
                }

                if (!isInCreateFlow) {
                    return;
                }

                lifeCycleEvents.signalCreated();
            }
        });
    }

    private void setupInteroperability() {
        webViewMessageDispatcher = new Html5WebViewMessageDispatcher(webView);
        eventHub.subscribe(new PatientPrivacyPolicyRequestHandler(this, webViewMessageDispatcher));
        eventHub.subscribe(new DeviceInformationRequestHandler(this, webViewMessageDispatcher));
        eventHub.subscribe(new OpenUrlRequestHandler(this));

        DeviceHandler deviceHandler = new DeviceHandler(this, webViewMessageDispatcher);
        eventHub.subscribe(new DeviceMeasurementRequestHandler(deviceHandler));
        eventHub.subscribe(new StopDeviceMeasurementRequestHandler(deviceHandler));

        eventHub.subscribe(new VideoEnabledRequestHandler(this, webViewMessageDispatcher));
        eventHub.subscribe(new StartNotificationSoundRequestHandler(this));
        eventHub.subscribe(new StopNotificationSoundRequestHandler(this));
        eventHub.subscribe(new StartVideoConferenceRequestHandler(this));

        eventHub.subscribe(new OverdueQuestionnairesRequestHandler(webViewMessageDispatcher));
        eventHub.subscribe(new SetupRemindersRequestHandler(this));
        eventHub.subscribe(new ClearQuestionnaireReminderRequestHandler(this));

        String script = "javascript:" +
                "sendMessageToNative = function(message) {" +
                "    var msgAsString = JSON.stringify(message);" +
                "    console.log('Sending message to native layer: ' + msgAsString);" +
                "    window.external.notify(msgAsString);"+
                "}";
        webView.loadUrl(script);
    }

    private List<JavaScriptInterface> generateJavaScriptInterfaces() {
        List<JavaScriptInterface> javaScriptInterfaces = new ArrayList<>();
        javaScriptInterfaces.add(new ErrorPageInterface(this, openTeleHtml5Url, webView));
        javaScriptInterfaces.add(eventHub);

        return javaScriptInterfaces;
    }

    private void startWebView() {
        Util.adjustScreenOrientation(this);
        webView.loadUrl(openTeleHtml5Url);
        setupInteroperability();
    }

    private boolean shouldExit(WebView webView) {
        return !webView.canGoBack() || webView.getUrl().endsWith(MENU_FRAGMENT);
    }

    @SuppressWarnings("rawtypes")
    public boolean clientIsVideoEnabled() {
        return ReflectionHelper.classCanBeLoaded(this, Constants.VIDEO_PROVIDER_QUALIFIED_NAME);
    }

    public void joinConference(VideoConference videoConference) {
        setVideoInProgress(true);

        //Insert video startup code here
    }

    public void stopNotificationSound() {
        Log.d(TAG, "Stopping notification sound");
        if (ringtone == null) {
            Log.w(TAG, "Ringtone is null when requested to stop sound");
            return;
        }
        ringtone.stop();
    }

    public void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            ringtone = RingtoneManager.getRingtone(this, notification);
            if (ringtone != null) {
                ringtone.play();
            }
        } catch (Exception ignored) {
            // TODO: Do we really want to always ignore it?
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Util.adjustScreenOrientation(this);

        if (!isInCreateFlow) {
            lifeCycleEvents.signalResumed();
        }
        isInCreateFlow = false;
    }

    @Override
    public void onBackPressed() {
        if (isVideoInProgress()) {
            // do nothing, so the user can't exit the video call
        } else if (shouldExit(webView)) {
            super.onBackPressed();
        } else {
            webView.goBack();
        }
    }

    @Override
    protected void onUserLeaveHint() {
        Log.d(TAG, "User leaving hint");
        stopNotificationSound();
        super.onUserLeaveHint();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Util.adjustScreenOrientation(this);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false; // disable menu button
    }

    public static boolean hasBeenCreated() {
        return hasBeenCreated;
    }

    public boolean isVideoInProgress() {
        return videoInProgress;
    }

    public void setVideoInProgress(boolean videoInProgress) {
        this.videoInProgress = videoInProgress;
    }
}

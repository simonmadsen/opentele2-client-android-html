package dk.silverbullet.telemed.reflection;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Method;

import dk.silverbullet.telemed.Util;

public class ReflectionHelper {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(ReflectionHelper.class);

    // --*-- Methods --*--

    public static boolean classCanBeLoaded(Context context, String packageAndClassName) {
        try {
            context.getClassLoader().loadClass(packageAndClassName);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static Method getMethod(Context context, String packageAndClassName,
                                   String methodName, Class... methodArgumentTypes)
            throws ReflectionHelperException {

        try {
            Class<?> clazz = loadClass(context, packageAndClassName);
            return clazz.getMethod(methodName, methodArgumentTypes);
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "Could not find method:'" + methodName
                    + "' in class:'"
                    + packageAndClassName + "'", e);
            throw new ReflectionHelperException(e);
        }
    }

    private static Class loadClass(Context context, String packageAndClassName)
            throws ReflectionHelperException {

        try {
            return context.getClassLoader().loadClass(packageAndClassName);
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Could not load class", e);
            throw new ReflectionHelperException(e);
        }
    }
}

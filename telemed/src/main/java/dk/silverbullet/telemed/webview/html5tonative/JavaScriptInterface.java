package dk.silverbullet.telemed.webview.html5tonative;

/**
 * Used as an interface between a WebView (e.g. to support an HTML5 layer) and
 * a local I/O functionality, i.e. blood pressure device integration or video
 * conference calls.
 */
public interface JavaScriptInterface {

    String getName();
}
